import java.util.Scanner;
public class Shop{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		
		Clothe[] shirt = new Clothe[4];
		for(int i=0 ; i < shirt.length ; i++){
			shirt[i]=new Clothe();
			System.out.println("What size of shirt do you have? ");
			shirt[i].size = input.nextLine();
			System.out.println("Where are these Shirts made from?");
			shirt[i].madeFrom = input.nextLine();
			System.out.println("How many do you have in stock?");
			shirt[i].stock = Integer.parseInt(input.nextLine());
		}
		System.out.println("The last shirt size is: "+shirt[3].size+" it is made from: "+shirt[3].madeFrom+" and we have this many in stock: "+shirt[3].stock);
		
		shirt[3].countStock();
		
	}
}